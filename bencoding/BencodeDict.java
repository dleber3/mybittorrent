/**
 * 
 */
package bencoding;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;

/**
 * @author David
 *
 */
public class BencodeDict extends BencodeItem {
	HashMap<BencodeString, BencodeItem> items;

	public BencodeDict() {
		items = new HashMap<BencodeString, BencodeItem>();
	}
	
	/**
	 * @param message
	 * @return
	 */
	public static BencodeDict parseDict(BencodedFile message) {
		
		BencodeDict list = new BencodeDict();
		message.startAt(1);
		
		while (message.charAt(0)!= 'e') {
			BencodeString string = (BencodeString) BencodeItem.parse(message);
			BencodeItem item = BencodeItem.parse(message);
			list.add(string, item);
		}
		
		message.startAt(1);
		
		return list;
	}
	
	public void add(BencodeString key, BencodeItem item) {
		items.put(key, item);
	}
	
	public String toString() {
		return items.toString();
	}
	
	/**
	 * @return
	 */
	public Hashtable<String, Object> toObject() {
		Hashtable<String, Object> output = new Hashtable<String, Object>();
		
		for (BencodeString curString : items.keySet()) {
			String key = curString.toString();
			Object item = items.get(curString).toObject();
			output.put(key, item);
		}
		
		return output;
	}


}
