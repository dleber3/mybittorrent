/**
 * 
 */
package bencoding;

/**
 * @author David
 *
 */
public class BencodeString extends BencodeItem {

	String val;
	
	public BencodeString(String val) {
		this.val = val;
	}
	
	public String toString() {
		return val;
	}

	/**
	 * @param message
	 * @return
	 */
	public static BencodeString parseString(BencodedFile message) {
		int colonLoc = message.indexOf(':');
		String length = message.substring(0, colonLoc);
		
		int numChars = Integer.parseInt(length);
		BencodeString newItem = new BencodeString(message.substring(colonLoc+1, colonLoc+1+numChars));
		message.startAt(colonLoc+1+numChars);
		return newItem;
	}
	
	public Object toObject() {
		return val;
	}
}
