/**
 * 
 */
package bencoding;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Scanner;

/**
 * @author David
 *
 */
public class BencodedFile {

	BencodeDict items;
	String message;
	
	boolean isDecoded = false;
	
	public BencodedFile(String fileContents)  {
		items = null;
		this.message = fileContents;
	}
	
	public void decode() {
		try {
			while (message.length()>0) {
				items = (BencodeDict) BencodeItem.parse(this);
			}
		} catch (RuntimeException e) {
			System.out.println("Exception caught.");
			this.isDecoded = true;
			System.out.println(this);
			throw e;
		}
		this.isDecoded = true;
	}
	
	public Hashtable<String, Object> toTable() {
		if (!isDecoded) {
			this.decode();
		}
		
		return items.toObject();
	}

	/**
	 * @param i
	 * @return
	 */
	public int charAt(int i) {
		return message.charAt(i);
	}

	/**
	 * @param c
	 * @return
	 */
	public int indexOf(char c) {
		return message.indexOf(c);
	}

	/**
	 * @param start
	 * @param end
	 * @return
	 */
	public String substring(int start, int end) {
		return message.substring(start, end);
	}
	
	public String toString() {
		if (this.isDecoded) {
			return items.toString();
		} else {
			return message;
		}
	}

	/**
	 * @param i
	 */
	public void startAt(int i) {
		if (i==message.length()) {
			message = "";
		} else {
			message = message.substring(i);
		}
	}

	
}
