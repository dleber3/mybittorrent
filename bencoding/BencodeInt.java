/**
 * 
 */
package bencoding;

/**
 * @author David
 *
 */
public class BencodeInt extends BencodeItem {

	int val;
	
	public BencodeInt(int val) {
		this.val = val;
	}
	
	public static BencodeInt parseInt(BencodedFile message) {
		int start = 1;
		int end = message.indexOf('e');
		String number = message.substring(start, end);
		message.startAt(end+1);
		return new BencodeInt(Integer.parseInt(number));
	}
	
	public Object toObject() {
		return new Integer(val);
	}
	
	public String toString() {
		return ""+val;
	}
	
}
