/**
 * 
 */
package bencoding;

import java.util.ArrayList;

/**
 * @author David
 *
 */
public class BencodeList extends BencodeItem {

	ArrayList<BencodeItem> items;
	
	public BencodeList() {
		items = new ArrayList<BencodeItem>();
	}

	/**
	 * @param message
	 * @return
	 */
	public static BencodeList parseList(BencodedFile message) {
		
		BencodeList list = new BencodeList();
		message.startAt(1);
		
		while (message.charAt(0)!= 'e') {
			list.add(BencodeItem.parse(message));
		}
		
		message.startAt(1);
		
		return list;
	}
	
	public Object toObject() {
		ArrayList<Object> output = new ArrayList<Object>();
		
		for (BencodeItem cur : items) {
			output.add(cur.toObject());
		}
		
		return output;
	}
	
	public void add(BencodeItem item) {
		items.add(item);
	}
	
	public String toString() {
		return items.toString();
	}
	
}
