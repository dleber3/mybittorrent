/**
 * 
 */
package bencoding;

/**
 * @author David
 *
 */
public abstract class BencodeItem {
	
	
	public abstract Object toObject();
	
	/**
	 * 
	 */
	public static BencodeItem parse(BencodedFile message) {
		BencodeItem item = null;
		switch (message.charAt(0)) {
			case 'i':
				item = BencodeInt.parseInt(message);
				break;
			case 'l':
				item = BencodeList.parseList(message);
				break;
			case 'd':
				item = BencodeDict.parseDict(message);
				break;
			default: 
				item = BencodeString.parseString(message);
		}
		
		return item;
	}
}
