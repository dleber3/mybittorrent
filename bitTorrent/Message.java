/**
 * 
 */
package bitTorrent;

import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;

/**
 * @author David
 *
 */
public class Message {

	private ByteBuffer message;
	
	public Message() {
	}
	
	/**
	 * @param returnedShake
	 */
	public Message(ByteBuffer message) {
		this.message = message;
	}

	public void setMessage(byte[] msg) {
		message = ByteBuffer.wrap(msg);
	}
	
	public ByteBuffer getMessage() {
		return message;
	}

	public String toString() {
		byte[] bytes = message.array();
	    StringBuilder sb = new StringBuilder();
	    for (byte b : bytes) {
	        sb.append(String.format("%02X ", b));
	    }
	    return sb.toString();
	}
}
