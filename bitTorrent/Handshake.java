/**
 * 
 */
package bitTorrent;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Hashtable;

/**
 * @author David
 *
 */
public class Handshake extends Message {
	
	public Handshake(Hashtable<String, Object> torrentFile) {
		//Handshake
		byte[] handshake = new byte[68];
		//1. 20 bytes (19 + "BitTorrent Protocol")
		handshake[0] = 19;
		int count=0;
		byte[] btp = "Bittorrent protocol".getBytes();
		for (int i=0; i<btp.length; i++) {
			handshake[i+1] = btp[i];
			count = i;
		}
		//2. 8 bytes - all zero
		for (int i=count; i<count+8; i++) {
			handshake[i] = 0;
		}
		count = count+8;
		
		//3. 20 Bytes SHA1 of the bencoded info value in metafile
		
		String sha1 = "d4b338e1d2b6eef678f654f99d3a7aa7d95691e7";
		int i=28;
		handshake[i++] = (byte) 212;	//d4
		handshake[i++] = (byte) 179;	//b3
		handshake[i++] = (byte) 56;		//38
		handshake[i++] = (byte) 225;	//e1
		handshake[i++] = (byte) 210;	//d2
		handshake[i++] = (byte) 182;	//b6
		handshake[i++] = (byte) 238;	//ee
		handshake[i++] = (byte) 246;	//f6
		handshake[i++] = (byte) 120;	//78
		handshake[i++] = (byte) 246;	//f6
		handshake[i++] = (byte) 84;		//54
		handshake[i++] = (byte) 249;	//f9
		handshake[i++] = (byte) 157;	//9d
		handshake[i++] = (byte) 58;		//3a
		handshake[i++] = (byte) 122;	//7a
		handshake[i++] = (byte) 167;	//a7
		handshake[i++] = (byte) 217;	//d9
		handshake[i++] = (byte) 86;		//56
		handshake[i++] = (byte) 145;	//91
		handshake[i++] = (byte) 231;	//e7
		
		//4. 20 bytes Peer ID used to identify this client
		for (i=48; i<68; i++) {
			handshake[i] = 1;
		}
		
		handshake[67] = (byte) '\n';
		this.setMessage(handshake);
		System.out.println(this);
	}

	/**
	 * @param returnedShake
	 */
	public static void validate(ByteBuffer returnedShake) {
		System.out.print("Returned shake: ");
		System.out.println(new Message(returnedShake));
		// TODO Auto-generated method stub
		throw new RuntimeException("NYI");
	}
	
}
