/**
 * 
 */
package bitTorrent;

import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Hashtable;

/**
 * @author David
 *
 */
public class BitClient {

	boolean isChoked = false;
	boolean isInterested = true;
	
	Hashtable<String, Object> torrentFile;
	SocketChannel channel;
	
	public BitClient(Machine machine, Hashtable<String, Object> torrentfile) {
		this(machine.getIP(), machine.getPort(), torrentfile);
	}
	
	public BitClient(String host, int port, Hashtable<String, Object> torrentFile) {
		this.torrentFile = torrentFile;
		InetSocketAddress address = new InetSocketAddress(host, port);
		try {
			channel = SocketChannel.open();
			System.out.println("Opened channel. Connecting...");
			channel.configureBlocking(true);
			boolean success = channel.connect(address);
			if (!success) {
				channel.close();
				throw new RuntimeException("Couldn't connect");
			}
			System.out.println("Connected to "+address);
			
			
			success = this.handShake();
			if (!success) {
				throw new RuntimeException("Handshake failed.");
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public boolean handShake() throws IOException {
		System.out.println("Starting shake");
		Handshake shake = new Handshake(torrentFile);
		this.send(shake);
		System.out.println("Shake sent. Waiting for reply.");
		
		ByteBuffer returnedShake = ByteBuffer.allocate(68);
		channel.read(returnedShake);
		System.out.println("Shake received. Validating...");
		Handshake.validate(returnedShake);
		System.out.println("Ready for normal operation.");
		
		return false;
	}
	
	public void send(Message msg) throws IOException {
		channel.write(msg.getMessage());
	}
	
}
