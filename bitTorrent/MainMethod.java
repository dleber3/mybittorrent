/**
 * 
 */
package bitTorrent;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Scanner;

import bencoding.BencodedFile;

/**
 * @author David
 *
 */
public class MainMethod {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		String message = getFile("4096.PNG.torrent");
		
		BencodedFile test = new BencodedFile(message);
		test.decode();
		Hashtable<String, Object> test2 = test.toTable();
		
		System.out.println(test2);
		System.out.println("Read file. Setting up client...");
				
		BitClient client = new BitClient(Machine.Nick, test2);
		System.out.println("Client established. Sending handshake...");
		client.handShake();
	}
	
	public static String getFile(String fileName) throws FileNotFoundException {
		Scanner file = new Scanner(new File("4096.PNG.torrent"));
		file.useDelimiter(":");
		
		if (!file.hasNext()) {
			throw new RuntimeException("File is empty?");
		}
		
		String message = file.next();
		while (file.hasNext()) {
			message+=":"+file.next();
		}
		return message;
	}

}
