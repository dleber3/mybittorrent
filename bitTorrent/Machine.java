/**
 * 
 */
package bitTorrent;

/**
 * @author David
 *
 */
public enum Machine {
	
	Laptop ("143.215.101.143", 50470),
	Desktop ("128.61.105.49", 34984),
	Desktop2 ("127.0.0.1", 4111),
	Nick ("128.61.87.150", 34767); //34767? 56436
	
	String ip;
	int port;
	
	private Machine(String ip, int port) {
		this.ip = ip;
		this.port = port;
	}
	
	public String getIP() {
		return ip;
	}
	
	public int getPort() {
		return port;
	}

}
