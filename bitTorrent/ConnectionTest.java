/**
 * 
 */
package bitTorrent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 * @author David
 *
 */
public class ConnectionTest {

	public static void main(String[] args) throws IOException {
		ServerSocket sock = new ServerSocket(4111);
		System.out.println("Waiting for connection...");
		Socket clientSocket = sock.accept();
		PrintWriter out = new PrintWriter(clientSocket.getOutputStream());
		BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		System.out.println("Connected!");
		
		String message = in.readLine();
		
		if (message.equals("Hello, Laptop.")) {
			out.write("Hello, Desktop.");
		} else {
			clientSocket.close();
			sock.close();
			throw new RuntimeException("Wrong message: "+message);
		}
	}
	
}
